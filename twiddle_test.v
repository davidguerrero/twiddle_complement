`timescale 1ns / 1ps

`include "twiddle_calculator.v"
`include "test_ROM.v"


module twiddle_test;
	reg myclk;
	wire real_sign,imaginary_sign,control_real_sign,control_imaginary_sign;
	wire [-1:-`output_width] real_magnitude,imaginary_magnitude,control_real_magnitude,control_imaginary_magnitude;
	reg [`samples_exponent-1:0] sample_number;

	twiddle_calculator unit_under_test
	(	sample_number,
		imaginary_sign,
		imaginary_magnitude,
		real_sign,
		real_magnitude
	);
     
	test_ROM my_test_ROM
	(	{control_imaginary_sign,control_imaginary_magnitude,control_real_sign,control_real_magnitude},
		sample_number
	);



	always #4 myclk = ~myclk;
	always	@(posedge myclk)
		sample_number<=sample_number+1;

	always	@(negedge myclk)
		begin
			if(control_real_sign!==real_sign && |real_magnitude)
		 		$display("angle_number=%h control_real_sign=%h real_sign=%h",sample_number,control_real_sign,real_sign);
			if(control_real_magnitude!==real_magnitude)
		 		$display("angle_number=%h control_real_magnitude=%h real_magnitude=%h",sample_number,control_real_magnitude,real_magnitude);
			if(control_imaginary_sign!==imaginary_sign && |imaginary_magnitude)
		 		$display("angle_number=%h control_imaginary_sign=%h imaginary_sign=%h",sample_number,control_imaginary_sign,imaginary_sign);
			if(control_imaginary_magnitude!==imaginary_magnitude)
		 		$display("angle_number=%h control_imaginary_magnitude=%h imaginary_magnitude=%h",sample_number,control_imaginary_magnitude,imaginary_magnitude);
		end
	initial begin
			//$dumpfile("twiddle_waves.vcd"); $dumpvars(0, twiddle_test);
			myclk=1'b1;
			sample_number=0;
			repeat (1<<`samples_exponent)	@(posedge myclk);
			$finish;
		end
endmodule

		
