import mpmath
guard_bits=80

def root_of_unity( samples_exponent,index_exponent,order_number,precision=mpmath.mp.prec):
    """Returns a ( 2^samples_exponent)-th root of unity.
        The roots are ordered in counterclockwise order and indexed starting from 0.
        The returned root is the one with index order_number*(2^index_exponent).
    """
    with mpmath.workprec(precision):
        return mpmath.expjpi(mpmath.ldexp(order_number,index_exponent+1-samples_exponent))

class fixed_point_root_of_unity:
    """Codes a ( 2^samples_exponent)-th root of unity in two integer fields.
        The roots are ordered in counterclockwise order and indexed starting from 0.
        The returned root x is the one with index order_number*(2^index_exponent).
        The field 'sin' is the nearest integer to the value imaginary_part(x)*(2^fractional_bits)
        The field 'complement' is the nearest integer to the value (1-real_part(x))*(2^fractional_bits)
    """
    def __init__(self,samples_exponent,index_exponent,order_number,fractional_bits=mpmath.mp.prec):
        root=root_of_unity(samples_exponent,index_exponent,order_number,fractional_bits+guard_bits)
        self.sin=int(mpmath.nint(mpmath.ldexp(root.imag,fractional_bits),prec=0))
        self.complement=int(mpmath.nint(mpmath.ldexp(1-root.real,fractional_bits),prec=0))
        
def fixed_point_root_of_unity_calculator( samples_exponent,index_exponent,fractional_bits=mpmath.mp.prec):
    """Returns a function that return a fixed point representation of a ( 2^samples_exponent)-th root of unity.
        The roots are ordered in counterclockwise order and indexed starting from 0.
        The function has an only parameter: order_number
        The returned root has the index order_number* (2^index_exponent).
    """
    
    def f(order_number):
        return fixed_point_root_of_unity(samples_exponent,index_exponent,order_number,fractional_bits)
    return f

def fixed_point_roots_of_unity( samples_exponent,index_exponent,number_of_roots_exponent,fractional_bits=mpmath.mp.prec):
    """Returns a list of fixed point representations of ( 2^samples_exponent)-th roots of unity.
        Lets be x one of the roots, its value is coded in a tuple (i,r)
        where i is an integer with the value imaginary_part(x)*(2^fractional_bits)
        and r is an integer with the value real_part(x)*(2^fractional_bits)
        The number of ( 2^samples_exponent)-th roots of unity roots is 2^samples_exponent,
        but only 2^number_of_roots_exponent are returned.
        The returned roots are the first whose index are multiple of (2^index_exponent) in counterclockwise order.
    """
    f=fixed_point_root_of_unity_calculator(samples_exponent,index_exponent,fractional_bits)
    return [f(i) for i in xrange(1<<number_of_roots_exponent)]
                      
class fixed_point_complex_product:
    """
    """
    def __init__(self,number_of_output_bits,number_of_input_bits,A,B):
        sin_A_plus_B=((A.sin+B.sin)<<number_of_input_bits)-A.sin*B.complement-B.sin*A.complement        
        complement_A_plus_B=((A.complement+B.complement)<<number_of_input_bits)-A.complement*B.complement+B.sin*A.sin
        displacements=(number_of_input_bits<<1)-number_of_output_bits
        if(displacements>0):
            lost_bit_position=1<<(displacements-1)
            sin_A_plus_B=sin_A_plus_B+ (sin_A_plus_B & lost_bit_position)
            sin_A_plus_B=sin_A_plus_B>>displacements
            complement_A_plus_B=complement_A_plus_B+ (complement_A_plus_B & lost_bit_position)        
            complement_A_plus_B=complement_A_plus_B>>displacements
        self.sin=sin_A_plus_B
        self.complement=complement_A_plus_B

class interval:
    """
    """
    def __init__(self,minimun,maximun):
        self.minimun=minimun
        self.maximun=maximun   

class tree:
    """
    """
    def __init__(self,precission,address_lines_interval,roots,depth,id):
        self.precission=precission
        self.address_lines_interval=address_lines_interval
        self.roots=roots
        self.id=id
        self.depth=depth
        self.total_address_lines=address_lines_interval.maximun-address_lines_interval.minimun+1
        self.number_of_roots=1<<self.total_address_lines
        last_root=roots[self.number_of_roots-1]
        self.sin_width=last_root.sin.bit_length()        
        self.complement_width=last_root.complement.bit_length()
        self.sin_most_significat_bit_index=self.sin_width-precission-1
        self.complement_most_significat_bit_index=self.complement_width-precission-1
        
def equal_root(A,B):
    return A.sin==B.sin and A.complement==B.complement

def generate_tree(samples_exponent,precission,address_lines_interval,roots,depth,id):
    x=tree(precission,address_lines_interval,roots,depth,id)
    if depth==0:
        x.cost=x.number_of_roots*(x.sin_width+x.complement_width)
    else:
        number_of_memories=1<<depth
        number_of_big_memories=x.total_address_lines % number_of_memories
        minimun_number_of_address_lines_per_son= (x.total_address_lines-number_of_big_memories)>>1

        for number_of_big_memories_of_son0 in xrange(number_of_big_memories+1):
            number_of_address_lines_son0=minimun_number_of_address_lines_per_son+number_of_big_memories_of_son0
            number_of_address_lines_son1=minimun_number_of_address_lines_per_son+number_of_big_memories-number_of_big_memories_of_son0
            son0_interval=interval(address_lines_interval.minimun,address_lines_interval.minimun+number_of_address_lines_son0-1)
            son1_interval=interval(son0_interval.maximun+1,address_lines_interval.maximun)
            
            son_precission=precission
            enougth_precission=False
    
            while not enougth_precission:
                enougth_precission=True
                son_precission=son_precission+1
                son0_roots=[]
                son1_roots=[]
                for son1_root_number in xrange(1<<number_of_address_lines_son1):
                    son1_current_root=fixed_point_root_of_unity(samples_exponent,son1_interval.minimun,son1_root_number,son_precission)
                    son1_roots.append(son1_current_root)
                    for son0_root_number in xrange(1<<number_of_address_lines_son0):
                        if(son1_root_number==0):
                            son0_current_root=fixed_point_root_of_unity(samples_exponent,son0_interval.minimun,son0_root_number,son_precission)
                            son0_roots.append(son0_current_root)
                        else:
                            son0_current_root=son0_roots[son0_root_number]
                        obtained_product=fixed_point_complex_product(precission,son_precission,son0_current_root,son1_current_root)
                        if not equal_root(obtained_product,roots[(son1_root_number<<number_of_address_lines_son0)+son0_root_number]):
                            enougth_precission=False
                            break
                    if not enougth_precission:
                        break
            son_depth=depth-1
            son0=generate_tree(samples_exponent,son_precission,son0_interval,son0_roots,son_depth,id+'_0')
            son1=generate_tree(samples_exponent,son_precission,son1_interval,son1_roots,son_depth,id+'_1')
            sons_cost=son0.cost+son1.cost
            if(number_of_big_memories_of_son0==0 or x.cost>sons_cost):
                x.son0=son0
                x.son1=son1
                x.cost=sons_cost
    return x
  
def define_rom_module(leaf,path_name):
    module_name='ROM'+leaf.id
    f = open(path_name+'/'+module_name+'.v', 'w')
    rom_width=leaf.complement_width+leaf.sin_width
    f.write("module "+module_name+"(output reg ["+str(rom_width-1)+":0] dout, input ["+str(leaf.total_address_lines-1)+":0] addr);\n\n")
    f.write("always @(addr) begin\n")
    f.write("    case (addr)\n")    
    last_address=(1<<leaf.total_address_lines)-1
    complement_width=leaf.complement_width
    data_width=str(rom_width)
    for address in xrange(last_address+1):
        root=leaf.roots[address]
        f.write("        ")
        if address<last_address:
            f.write(str(address))
        else:
            f.write('default')
        f.write(": dout="+data_width+"'h"+format((root.sin<<complement_width)+root.complement,'x')+";\n")
    f.write("    endcase\n")
    f.write("end\n\n")
    f.write("endmodule")
    f.close()
    
def define_rom_modules(tree,path_name):
    if not tree.depth: #is a leaf
        define_rom_module(tree,path_name)
    else:
        define_rom_modules(tree.son0,path_name)
        define_rom_modules(tree.son1,path_name)
        
def verilog_define(name,value,verilog_file,sized=False):
    if sized:
        size=str(value.bit_length())+"'d"
    else:
        size=''
    verilog_file.write('`define '+name+" "+size+str(value)+'\n')
    
def verilog_include(included_filename,output_file):
    output_file.write('`include "'+included_filename+'.v"\n')

def verilog_wire(wire_name,left_index,right_index,output_file):
    output_file.write('wire ['+str(left_index)+':'+str(right_index)+'] '+wire_name+';\n')
    
def generate_trigonometric_calculator(tree,path_name):
    module_name="trigonometric_calculator"##+tree.id
    f = open(path_name+'/'+module_name+'.v', 'w')
    #if tree.depth: #has sons
    verilog_include("root_trigonometric_adder",f)
    if tree.depth>1: #has more than two sons
        verilog_include("trigonometric_adder",f)
    include_rom_descriptions(tree,f)
    f.write("module	"+module_name+'(	input ['+str(tree.total_address_lines-1)+":0] d, output [-1:"+str(-tree.precission)+"] sin,cosin);\n")
    #generate_wires(tree,f)
    verilog_wire("sin_"+tree.id,tree.sin_most_significat_bit_index,-tree.precission,f)    
    verilog_wire("minus_com_"+tree.id,-1,-tree.precission,f)
    generate_wires(tree.son0,f)
    generate_wires(tree.son1,f)
    
    f.write("assign sin=sin_"+tree.id+";\n")
    f.write("assign cosin= minus_com_"+tree.id+"[-1] ? minus_com_"+tree.id+" : -1 ;\n")
    #f.write("assign cosin={1'b1,~com_"+tree.id+" + |com_"+tree.id+"};\n")
    f.write("root_trigonometric_adder 	#("+str(tree.son0.precission)+","+str(tree.son0.sin_most_significat_bit_index)+","+str(tree.son0.complement_most_significat_bit_index)+","+str(tree.son1.sin_most_significat_bit_index)+","+str(tree.son1.complement_most_significat_bit_index)+","+str(tree.precission)+","+str(tree.sin_most_significat_bit_index)+") ")
    f.write("myroottrigonometric_adder"+tree.id)
    f.write("("+"sin_"+tree.son0.id+",com_"+tree.son0.id+",sin_"+tree.son1.id+",com_"+tree.son1.id+",sin_"+tree.id+",minus_com_"+tree.id+");\n")
    instantiate_internal_components(tree.son0,f)
    instantiate_internal_components(tree.son1,f)
    f.write("endmodule\n")
    with mpmath.workprec(tree.precission+guard_bits):
        inverse_sqrt_2=int(mpmath.nint(mpmath.ldexp(1/mpmath.sqrt(2),tree.precission),prec=0))
    verilog_define('inverse_sqrt_2',inverse_sqrt_2,f,True)
    verilog_define('samples_exponent',tree.total_address_lines+3,f)
    verilog_define('output_width',tree.precission,f)
    f.close()
    
def include_rom_descriptions(tree,f):
    if tree.depth: #has sons
        include_rom_descriptions(tree.son0,f)        
        include_rom_descriptions(tree.son1,f)
    else:
        verilog_include('ROM'+tree.id,f)
        
def generate_wires(tree,f):
    verilog_wire("sin_"+tree.id,tree.sin_most_significat_bit_index,-tree.precission,f)    
    verilog_wire("com_"+tree.id,tree.complement_most_significat_bit_index,-tree.precission,f)
    if tree.depth: #has sons
        generate_wires(tree.son0,f)        
        generate_wires(tree.son1,f)
        
def instantiate_internal_components(tree,f):
    if tree.depth:
        f.write("trigonometric_adder 	#("+str(tree.son0.precission)+","+str(tree.son0.sin_most_significat_bit_index)+","+str(tree.son0.complement_most_significat_bit_index)+","+str(tree.son1.sin_most_significat_bit_index)+","+str(tree.son1.complement_most_significat_bit_index)+","+str(tree.precission)+","+str(tree.sin_most_significat_bit_index)+","+str(tree.complement_most_significat_bit_index)+") ")
        f.write("mytrigonometric_adder"+tree.id)
        f.write("("+"sin_"+tree.son0.id+",com_"+tree.son0.id+",sin_"+tree.son1.id+",com_"+tree.son1.id+",sin_"+tree.id+",com_"+tree.id+");\n")
#	(	input	[Amaximun_sin_index:-Aprecission] sinA,
#			[-1:-Aprecission] cosinA,
#			[Bmaximun_sin_index:-Bprecission] sinB,
#			[-1:-Bprecission] cosinB,
#	 	output	[output_maximun_sin_index:-output_precission] sin,
#			[-1:-output_precission] cosin
#	);        
        
        instantiate_internal_components(tree.son0,f)
        instantiate_internal_components(tree.son1,f)
    else:
        f.write("ROM"+tree.id+" myrom"+tree.id+"({sin_"+str(tree.id)+",com_"+str(tree.id)+"},d["+str(tree.address_lines_interval.maximun)+":"+str(tree.address_lines_interval.minimun)+"]"+");\n")

from shutil import copyfile

import os

def generate_verilog_code(tree):
    dirname="twiddle_calculator_"+tree.id
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    define_rom_modules(tree,dirname)
#    if not os.path.exists(dirname):
#        raise "could not create directory "+dirname
#    os.chdir(dirname)
    copyfile("twiddle_calculator.v",dirname+"/twiddle_calculator.v")
    copyfile("root_trigonometric_adder.v",dirname+"/root_trigonometric_adder.v")
    if tree.depth>1: #has more than 2 sons
        copyfile("trigonometric_adder.v",dirname+"/trigonometric_adder.v")
    generate_trigonometric_calculator(tree,dirname)

import sys

class roots_generation(Exception):
    pass

def maximun_number_of_stages(number_of_memory_lines):
    return number_of_memory_lines.bit_length()-1 #this is floor(log2(number_of_memory_lines))

def generate_verilog_files(samples_exponent,fractional_bits,number_of_stages):
    if samples_exponent<4:
        raise roots_generation('There must be at least 16 samples')
    if fractional_bits<1:
        raise roots_generation('The precision can not be lower than 1')
    number_of_input_lines=samples_exponent-3
    maximun_number_of_stages=number_of_input_lines.bit_length()-1 #this is floor(log2(number_of_input_lines))
    if number_of_stages>maximun_number_of_stages:
        raise roots_generation('The maximun number of stages for this number of samples is '+str(maximun_number_of_stages))
    if number_of_stages<1:
        raise roots_generation('The minimun number of stages is 1')
        
    roots=fixed_point_roots_of_unity(samples_exponent,0,samples_exponent-3,fractional_bits)
    t=generate_tree(samples_exponent,fractional_bits,interval(0,number_of_input_lines-1),roots,number_of_stages,str(samples_exponent)+"_"+str(fractional_bits)+"_"+str(number_of_stages))
    generate_verilog_code(t)

try:
    generate_verilog_files(int(sys.argv[1]),int(sys.argv[2]),int(sys.argv[3]))
except IndexError:
    print "arguments: <samples exponent> <fractional bits> <number of stages>\n"
