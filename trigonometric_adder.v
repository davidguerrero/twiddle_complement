module	trigonometric_adder
	#(parameter	input_precission=0,
			Amaximun_sin_index=0,
			Amaximun_com_index=0,
			Bmaximun_sin_index=0,
			Bmaximun_com_index=0,
			output_precission=0,
			output_maximun_sin_index=0,			
			output_maximun_com_index=0
			)
	(	input	[Amaximun_sin_index:-input_precission] sinA,
		input	[Amaximun_com_index:-input_precission] comA,
		input	[Bmaximun_sin_index:-input_precission] sinB,
		input	[Bmaximun_com_index:-input_precission] comB,
	 	output	[output_maximun_sin_index:-output_precission] sin,
		output	[output_maximun_com_index:-output_precission] com
	);
	wire	[Amaximun_sin_index+Bmaximun_com_index+1:-2*input_precission] sinAcomB;
	wire	[Amaximun_com_index+Bmaximun_sin_index+1:-2*input_precission] comAsinB;
	wire	[Amaximun_sin_index+Bmaximun_sin_index+1:-2*input_precission] sinAcomB_PLUS_comAsinB;

	wire	[Amaximun_com_index+Bmaximun_com_index+1:-2*input_precission] comAcomB;
	wire	[Amaximun_sin_index+Bmaximun_sin_index+1:-2*input_precission] sinAsinB;
	wire	[Amaximun_sin_index+Bmaximun_sin_index+1:-2*input_precission] sinAsinB_MINUS_comAcomB;

	wire 	[output_maximun_sin_index:-input_precission] accurate_sin;
	wire	[output_maximun_com_index:-input_precission] accurate_com;

	assign sinAcomB=sinA*comB;
	assign comAsinB=comA*sinB;
	assign comAcomB=comA*comB;
	assign sinAsinB=sinA*sinB;

	assign sinAcomB_PLUS_comAsinB=sinAcomB+comAsinB;
	assign sinAsinB_MINUS_comAcomB=sinAsinB-comAcomB;

	assign accurate_sin=sinA+sinB-sinAcomB_PLUS_comAsinB[Amaximun_sin_index+Bmaximun_sin_index+1:-input_precission]-|sinAcomB_PLUS_comAsinB[-input_precission-1:-2*input_precission];
	assign accurate_com=comA+comB+sinAsinB_MINUS_comAcomB[Amaximun_sin_index+Bmaximun_sin_index+1:-input_precission];

	assign sin=accurate_sin[output_maximun_sin_index:-output_precission]+accurate_sin[-output_precission-1];
	assign com=accurate_com[output_maximun_com_index:-output_precission]+accurate_com[-output_precission-1];
endmodule

