module register #(parameter width=1)
		(
			input clk,
			input [width-1:0] inputs,
			output reg [width-1:0] outputs
		);

	always @(posedge clk)
		outputs <= inputs;
endmodule

`include "twiddle_calculator.v"

module top_level
	(	input clk,
		input [`samples_exponent-1:0] inputs,
		output [(`output_width+1)*2-1:0] outputs
	);

	wire [`samples_exponent-1:0] sampled_inputs;
	wire [(`output_width+1)*2-1:0] unsampled_outputs;

	register #(`samples_exponent) input_register(clk,inputs,sampled_inputs);
	register #((`output_width+1)*2) output_register(clk,unsampled_outputs,outputs);
	
	twiddle_calculator unit_analisys
	(	
		sampled_inputs,
		unsampled_outputs [`output_width*2],
		unsampled_outputs [`output_width-1:0],
		unsampled_outputs [`output_width*2+1],
		unsampled_outputs [`output_width*2-1:`output_width]
	);
endmodule
