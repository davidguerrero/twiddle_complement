module	root_trigonometric_adder
	#(parameter	input_precission=0,
			Amaximun_sin_index=0,
			Amaximun_com_index=0,
			Bmaximun_sin_index=0,
			Bmaximun_com_index=0,
			output_precission=0,
			output_maximun_sin_index=0/*,			
			output_maximun_com_index=0*/
			)
	(	input	[Amaximun_sin_index:-input_precission] sinA,
		input	[Amaximun_com_index:-input_precission] comA,
		input	[Bmaximun_sin_index:-input_precission] sinB,
		input	[Bmaximun_com_index:-input_precission] comB,
	 	output	[output_maximun_sin_index:-output_precission] sin,
		output	[-1:-output_precission] minus_com
	);
	wire	[Amaximun_sin_index+Bmaximun_com_index+1:-2*input_precission] sinAcomB;
	wire	[Amaximun_com_index+Bmaximun_sin_index+1:-2*input_precission] comAsinB;
	wire	[Amaximun_sin_index+Bmaximun_sin_index+1:-2*input_precission] sinAcomB_PLUS_comAsinB;

	wire	[Amaximun_com_index+Bmaximun_com_index+1:-2*input_precission] comAcomB;
	wire	[Amaximun_sin_index+Bmaximun_sin_index+1:-2*input_precission] sinAsinB;
	wire	[-1:-2*input_precission] comAcomB_MINUS_sinAsinB;

	wire 	[output_maximun_sin_index:-input_precission] accurate_sin;
	wire	[-1:-input_precission] accurate_minus_com;

	assign sinAcomB=sinA*comB;
	assign comAsinB=comA*sinB;
	assign comAcomB=comA*comB;
	assign sinAsinB=sinA*sinB;

	assign sinAcomB_PLUS_comAsinB=sinAcomB+comAsinB;
	assign comAcomB_MINUS_sinAsinB=comAcomB-sinAsinB;

	assign accurate_sin=sinA+sinB-sinAcomB_PLUS_comAsinB[Amaximun_sin_index+Bmaximun_sin_index+1:-input_precission]-|sinAcomB_PLUS_comAsinB[-input_precission-1:-2*input_precission];
	assign accurate_minus_com=comAcomB_MINUS_sinAsinB[-1:-input_precission]-(comA+comB);

	assign sin=accurate_sin[output_maximun_sin_index:-output_precission]+accurate_sin[-output_precission-1];
	assign minus_com=accurate_minus_com[-1:-output_precission]+accurate_minus_com[-output_precission-1];
endmodule

